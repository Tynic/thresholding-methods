﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSpokus
{
    class TH_EntrHist
    {
        public static int TH;

        public static byte[,] entrHist(byte[,] grayArray, int[] histArray)
        {
            //výsledné pole s vypočítanými obrazovými body
            byte[,] resultArray = new byte[grayArray.GetLength(0), grayArray.GetLength(1)];

            double[] normHistogram = new double[256];
            double[] cumHistogram = new double[256];
            long AllPixels = grayArray.LongLength;

            // normovaný histogram
            for (int j = 0; j < 256; j++)
            {
                normHistogram[j] = histArray[j] / (double)AllPixels; 
            }

            // první složka kumulovaného histogramu, suma = 1
            cumHistogram[0] = normHistogram[0];

            // kumulovaný histogram
            for (int j = 1; j < 256; j++)
            {
                cumHistogram[j] = normHistogram[j];
                cumHistogram[j] += cumHistogram[j - 1];
            }
            
            double H = 0;
            double maxp = double.MinValue;
            double[] maxpi = new double[256];
            double[] maxpj = new double[256];
            double ap = 0;
            //vypočtení apriorní entropie, max(p1 ... pk) , max(pk+1 ... pn)
            for (int k = 0; k < 256; k++)
            {
                ap = ((double)normHistogram[k] / cumHistogram[255]);
                if (ap != 0)
                {
                    H -= (ap * Math.Log(ap, 2));
                }
                if (ap > maxp)
                {
                    maxp = ap;

                }
                maxpi[k] = maxp / cumHistogram[255];
            }
            for (int k = 255; k >= 0; k--)
            {
                ap = ((double)normHistogram[k] / cumHistogram[255]);
                if (ap > maxp)
                {
                    maxp = ap;

                }
                maxpj[k] = maxp / cumHistogram[255];
            }


            double[] F = new double[100];
            double maxF = double.MinValue;
            int i = 0;
            double maxAlpha = 0;
            //vypocet kriteriální fce
            for (double alpha = 0.01; alpha < 1; alpha += 0.01, i++)
            {
                double sum;
                int k = 0;
                //vypočtu k
                for (sum = 0; sum > -alpha * H && k < 256; k++)
                {
                    if (normHistogram[k] == 0) continue;
                    sum += (((double)normHistogram[k] / cumHistogram[255]) * Math.Log(((double)normHistogram[k] / cumHistogram[255]), 2));
                }
                if (k == 256 || normHistogram[k] == 0 || maxpi[k] == 0 || maxpj[k] == 0 || cumHistogram[255] - cumHistogram[k] == 0) continue; //zabraneni deleni 0 a log(0)
                //kriteriální fce
                F[i] = ((alpha * Math.Log((double)cumHistogram[k] / cumHistogram[255], 2)) / Math.Log(maxpi[k], 2)) + (((1 - alpha) * Math.Log((double)(cumHistogram[255] - cumHistogram[k]) / cumHistogram[255], 2)) / Math.Log(maxpj[k], 2));
                if (maxF < F[i]) { maxF = F[i]; maxAlpha = alpha; }

            }
            int entrHistTreshold;
            double sum2 = 0;
            //výpočet prahu
            for (entrHistTreshold = 0; entrHistTreshold < 256; entrHistTreshold++)
            {
                ap = ((double)normHistogram[entrHistTreshold] / cumHistogram[255]);
                if (ap != 0) sum2 += (ap * Math.Log(ap, 2));
                if (sum2 < -maxAlpha * H) break;

            }
            TH = entrHistTreshold;
            // obarvování bitmapy
            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    if (grayArray[x, y] >= entrHistTreshold)
                    {
                        resultArray[x, y] = (byte)255;
                    }
                    else
                    {
                        resultArray[x, y] = (byte)0;
                    }
                }
            }
            return resultArray;
        }
    }
}
