﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSpokus
{
    class TH_Otsu
    {
        // Hledá práh podle rozptylu mezi třídami
        public static int TH;
        public static byte[,] otsu(byte[,] grayArray, int[] histArray)
        {
            //výsledné pole s vypočítanými obrazovými body
            byte[,] resultArray = new byte[grayArray.GetLength(0), grayArray.GetLength(1)];

            double[] normHistogram = new double[256];
            double[] cumHistogram = new double[256];
            long AllPixels = grayArray.LongLength;

            // normovaný histogram
            for (int i = 0; i < 256; i++)
            {
                normHistogram[i] = histArray[i] / (double)AllPixels; //Normovani histogramu
            }

            // první složka kumulovaného histogramu, suma = 1
            cumHistogram[0] = normHistogram[0];

            // kumulovaný histogram
            for (int i = 1; i < 256; i++)
            {
                cumHistogram[i] = normHistogram[i];
                cumHistogram[i] += cumHistogram[i - 1];
            }

            double omega0 = 0;
            double omega1 = 0;
            double mi0 = 0;
            double mi1 = 0;
            double miT = 0;
            double miK = 0;
            double maxvariance = 0;
            int ixKMaxvariance = 0;
            double variance = 0;

            //Koeficient miT
            for (int i = 0; i < 256; i++)
                miT += (i + 1) * normHistogram[i];

            // Výpočet maximálního varianceu
            for (int ix = 1; ix < 256; ix++)
            {
                omega0 = cumHistogram[ix - 1];
                omega1 = 1 - omega0;
                miK = 0;
                for (int j = 0; j < ix; j++)
                {
                    miK += (j + 1) * normHistogram[j];
                }
                mi0 = miK / omega0;
                mi1 = (miT - miK) / omega1;
                // otsu rovnice
                variance = omega0 * omega1 * ((mi1 - mi0) * (mi1 - mi0));

                if (variance >= maxvariance)
                {
                    maxvariance = variance;
                    ixKMaxvariance = ix;
                }
            }
            byte otsuThreshold = (byte)ixKMaxvariance;

            TH = ixKMaxvariance;

            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    if (grayArray[x, y] >= otsuThreshold)
                    {
                        resultArray[x, y] = (byte)255;
                    }
                    else
                    {
                        resultArray[x, y] = (byte)0;
                    }
                }
            }
            return resultArray;

        }

        // Práh dle rozptylu mezi třídami + valley emphasis
        public static byte[,] valley(byte[,] grayArray, int[] histArray)
        {
            //výsledné pole s vypočítanými obrazovými body
            byte[,] resultArray = new byte[grayArray.GetLength(0), grayArray.GetLength(1)];

            double[] normHistogram = new double[256];
            double[] cumHistogram = new double[256];
            long AllPixels = grayArray.LongLength;

            // normovaný histogram
            for (int i = 0; i < 256; i++)
            {
                normHistogram[i] = histArray[i] / (double)AllPixels; //Normovani histogramu
            }

            // první složka kumulovaného histogramu, suma = 1
            cumHistogram[0] = normHistogram[0];

            // kumulovaný histogram
            for (int i = 1; i < 256; i++)
            {
                cumHistogram[i] = normHistogram[i];
                cumHistogram[i] += cumHistogram[i - 1];
            }

            double omega0 = 0;
            double omega1 = 0;
            double mi0 = 0;
            double mi1 = 0;
            double miT = 0;
            double miK = 0;
            double maxvariance = 0;
            int iKMaxvariance = 0;
            double variance = 0;
            double pt = 0.0;

            //Koeficient miT
            //střední hodnota
            for (int i = 0; i < 256; i++)
                miT += (i + 1) * normHistogram[i];      // suma jas * p(jas)

            // Výpočet maximálního rozptylu
            for (int i = 1; i < 256; i++)
            {
                omega0 = cumHistogram[i - 1];
                omega1 = 1 - omega0;
                miK = 0;
                for (int j = 0; j < i; j++)
                {
                    miK += (j + 1) * normHistogram[j];
                }
                mi0 = miK / omega0;
                mi1 = (miT - miK) / omega1;
                // otsu rovnice
                variance = omega0 * omega1 * ((mi1 - mi0) * (mi1 - mi0));

                // pravděpodobnost, že se vyskytne jas T
                pt = (double)normHistogram[i] / AllPixels;
                variance *= (1 - pt);

                if (variance >= maxvariance)
                {
                    maxvariance = variance;
                    iKMaxvariance = i;
                }

                TH = iKMaxvariance;
            }
            byte valleyThreshold = (byte)iKMaxvariance;

            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    if (grayArray[x, y] >= valleyThreshold)
                    {
                        resultArray[x, y] = (byte)255;
                    }
                    else
                    {
                        resultArray[x, y] = (byte)0;
                    }
                }
            }
            
            return resultArray;
        }

        // práh dle rozptylu uvnitř tříd
        public static byte[,] otsuInsideClass(byte[,] grayArray, int[] histArray)
        {
            //výsledné pole s vypočítanými obrazovými body
            byte[,] resultArray = new byte[grayArray.GetLength(0), grayArray.GetLength(1)];

            double[] normHistogram = new double[256];
            double[] cumHistogram = new double[256];
            long AllPixels = grayArray.LongLength;

            // normovaný histogram
            for (int i = 0; i < 256; i++)
            {
                normHistogram[i] = histArray[i] / (double)AllPixels; //Normovani histogramu
            }

            // první složka kumulovaného histogramu, suma = 1
            cumHistogram[0] = normHistogram[0];

            // kumulovaný histogram
            for (int i = 1; i < 256; i++)
            {
                cumHistogram[i] = normHistogram[i];
                cumHistogram[i] += cumHistogram[i - 1];
            }

            double[] mi = new double[256];
            double cumulMi = 0;

            //predvypocet stredni hodnoty trid
            for (int i = 0; i < 256; i++)
            {
                cumulMi += (double)normHistogram[i] / cumHistogram[255] * i;
                mi[i] = cumulMi;
            }        

            double maxSigma_b2 = 0;
            int otsuThreshold = 0;
            //průchod potenciálními prahy
            for (int k = 1; k < 255; k++)
            {
                double omega0 = (double)cumHistogram[k] / cumHistogram[255];
                double omega1 = 1 - omega0;
                double mi0 = mi[k] / omega0;
                double mi1 = (mi[255] - mi[k]) / omega1;

                double sigma_b2 = omega0 * mi0 * mi0 + omega1 * mi1 * mi1;
                if (maxSigma_b2 < sigma_b2)
                {
                    maxSigma_b2 = sigma_b2;
                    otsuThreshold = k;
                }

                TH = otsuThreshold;
            }
            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    if (grayArray[x, y] >= otsuThreshold)
                    {
                        resultArray[x, y] = (byte)255;
                    }
                    else
                    {
                        resultArray[x, y] = (byte)0;
                    }
                }
            }

            return resultArray;
        }

    }
}
