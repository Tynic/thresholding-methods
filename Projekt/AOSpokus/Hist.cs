﻿using System.Drawing;
using System.Linq;

namespace AOSpokus
{
    class Hist
    {
        public static int TH;
        public static int[] histogram(byte[,] grayArray)
        {
            // počítej jasy a jejich výskyt
            int[] histArray = new int[256];
            int frequency;

            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    frequency = grayArray[x, y];
                    histArray[frequency]++;
                }
            }

            return histArray;
        }

        public static Bitmap histBitmap(int[] histArray)
        {

            int histHeight = 128;
            float max = histArray.Max();
            Bitmap img = new Bitmap(256, histHeight + 10);
            // vykreslí histogram
            using (Graphics g = Graphics.FromImage(img))
            {
                for (int i = 0; i < histArray.Length; i++)
                {
                    float pct = histArray[i] / max;
                    g.DrawLine(Pens.Black,
                        new Point(i, img.Height),
                        new Point(i, img.Height -1- (int)(pct * histHeight))
                        );
                }

                if (Form1.TH > 0)
                {
                    TH = Form1.TH;
                    Brush aBrush = (Brush)Brushes.Red;
                    g.FillRectangle(aBrush, TH, img.Height -2- (int)(TH/max * histHeight), 4, 4);
                }
            }

            return img;
        }

      }
}
