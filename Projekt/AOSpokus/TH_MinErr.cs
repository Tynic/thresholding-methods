﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOSpokus
{
    class TH_MinErr
    {
        public static int TH;
        double[] jv;

        public static byte[,] minErr(byte[,] grayArray, int[] histArray)
        {
            byte[,] resultArray = new byte[grayArray.GetLength(0), grayArray.GetLength(1)];
            double[] normHistogram = new double[256];
            double[] cumHistogram = new double[256];
            long AllPixels = grayArray.LongLength;

            // normovaný histogram
            for (int i = 0; i < 256; i++)
            {
                normHistogram[i] = histArray[i] / (double)AllPixels; //Normovani histogramu
            }

            // první složka kumulovaného histogramu, suma = 1
            cumHistogram[0] = normHistogram[0];

            // kumulovaný histogram
            for (int i = 1; i < 256; i++)
            {
                cumHistogram[i] = normHistogram[i];
                cumHistogram[i] += cumHistogram[i - 1];
            }

            int minErrTreshold = 0;
            double p0 = 0;
            double p1 = 0;
            double mu0=0;
            double mu1=0;
            
            double sigma20 = 0;
            double sigma21 = 0;
            double[] jv=new double[256];

            //volba prahu, výpočet hodnot pro zvolení maxima
            for (int t = 0; t < 256; t++)
            {               
                p0 = (double)cumHistogram[t] / cumHistogram[255];
                p1=1.0 - p0;

                //str hodnota mu pro obe tridy
                mu0 = 0;
                for (int i = 0; i <= t; i++) { mu0 += (i+1) * ((double)cumHistogram[i] / cumHistogram[255]); }
                mu0/=p0;
                mu1 = 0;
                for (int i = t + 1; i < 256; i++) { mu1 += (i+1) * ((double)cumHistogram[i] / cumHistogram[255]); }
                mu1/=p1;

                //rozptyl sigma2
                sigma20 = 0;
                for (int i = 0; i <= t; i++)
                {
                    sigma20 += (i +1- mu0) * (i +1- mu0) * ((double)cumHistogram[i] / cumHistogram[255]);
                }

                sigma20 /= p0;
                sigma21 = 0;

                for (int i = t+1; i <= 255; i++)
                {
                    sigma21 += (i +1- mu1) * (i +1- mu1) * ((double)cumHistogram[i] / cumHistogram[255]);
                }
                sigma21 /= p1;

                //krit fce 
                jv[t] = 1 + 2*(p0 * Math.Log(Math.Sqrt(sigma20)) + p1 * Math.Log(Math.Sqrt(sigma21))) - 2*(p0 * Math.Log(p0) + p1 * Math.Log(p1));               
            }

            double dif = 0;
            double last=0;
            double status = 0;
            double min = double.MaxValue;
            
            //průchod kriteriální křivkou a hledání vnitřního minima
            for (int t = 4; t < 251; t++)
            {
                if (double.IsNegativeInfinity(jv[t]) || double.IsPositiveInfinity(jv[t])) continue;
                dif = jv[t] -last;
                last = jv[t];
                if (status == 0 && dif < 0) status = 1;
                if (status == 1 && dif > 0)
                {
                    status = 0;
                    if (jv[t] < min)
                    {
                        min = jv[t];
                        minErrTreshold= t;
                    }
                }
            }
            TH = minErrTreshold;

            for (int y = 0; y < grayArray.GetLength(1); y++)
            {
                for (int x = 0; x < grayArray.GetLength(0); x++)
                {
                    if (grayArray[x, y] >= minErrTreshold)
                    {
                        resultArray[x, y] = (byte)255;
                    }
                    else
                    {
                        resultArray[x, y] = (byte)0;
                    }
                }
            }
            return resultArray;

        }

    }
}
