﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;

namespace AOSpokus
{
    public partial class Form1 : Form
    {
        public static int[] histArrayG2 = new int[256];
        public static byte[,] grayArray;
        public static int TH;

        public Form1()
        {
            InitializeComponent();
        }

        private void openMenu(object sender, EventArgs e)
        {
            string ChosenFile = "";

            openFD.InitialDirectory = "C:";
            openFD.Title = "Vložte obrázek";
            openFD.FileName = "";
            openFD.Filter = "JPEG Images|*.jpg|PNG Images|*.png|BITMAPS|*.bmp";

            if (openFD.ShowDialog() != DialogResult.Cancel)
            {
                ChosenFile = openFD.FileName;
                PB1.Image = Image.FromFile(ChosenFile);
                PB2.Image = null;
                PB3.Image = null;
            }
        }

        private void saveMenu(object sender, EventArgs e)
        {
            if (PB3.Image == null)
            {
                MessageBox.Show("There is no output image to save.");
                return;
            }
            SaveFileDialog sfd1 = new SaveFileDialog();
            sfd1.Filter = "JPEG Images|*.jpg|PNG Images|*.png|BITMAPS|*.bmp";
            sfd1.Title = "Save an Image File";
            sfd1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (sfd1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs = (System.IO.FileStream)sfd1.OpenFile();

                switch (sfd1.FilterIndex)
                {

                    case 1:
                        PB3.Image.Save(fs, ImageFormat.Jpeg);
                        break;

                    case 2:
                        PB3.Image.Save(fs, ImageFormat.Png);
                        break;

                    case 3:
                        PB3.Image.Save(fs, ImageFormat.Bmp);
                        break;
                }
                fs.Close();
            }
        }
        private void greyscaleMenu(object sender, EventArgs e)
        {
            TH = 0;
            PB3.Image = null;
            if (PB1.Image != null)
            {
                Bitmap bitmap = new Bitmap(PB1.Image);
                byte[,,] bitmapArray = Process.getArray(bitmap);
                grayArray = Process.grayscale(bitmapArray);

                bitmap = Process.toBitmap(grayArray);
                PB2.Image = bitmap;

                histArrayG2 = Hist.histogram(grayArray);
            }
            
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void PB2_Click(object sender, EventArgs e)
        {

        }


        private void otsuBetwMenu(object sender, EventArgs e)
        {
            if (PB1.Image != null)
            {
                if (PB2.Image == null)
                {
                    greyscaleMenu(sender, e);
                }

                byte[,] otsuArray = TH_Otsu.otsu(grayArray, histArrayG2);
                Bitmap otsuBitmap = new Bitmap(PB2.Image);
                otsuBitmap = Process.toBitmap(otsuArray);
                PB3.Image = otsuBitmap;
                TH = TH_Otsu.TH;
                statusLabelBar(sender, e);
            }
        }

        private void PB2_Click_1(object sender, EventArgs e)
        {
            if (PB2.Image != null)
            {
                Bitmap histBitmap = Hist.histBitmap(histArrayG2);
                //Bitmap histBitmap = new Bitmap(Threshold.drawHistogram(histArrayG2));

                // Create a new instance of the Form2 class
                Histogram histForm2 = new Histogram(histBitmap);

                // Show the settings form
                histForm2.Show();
            }

        }

        private void valleyMenu(object sender, EventArgs e)
        {
            if (PB1.Image != null)
            {
                if (PB2.Image == null)
                {
                    greyscaleMenu(sender, e);

                }

                // valley
                byte[,] valleyArray = TH_Otsu.valley(grayArray, histArrayG2);
                Bitmap valleyBitmap = new Bitmap(PB2.Image);
                valleyBitmap = Process.toBitmap(valleyArray);
                PB3.Image = valleyBitmap;
                TH = TH_Otsu.TH;
                statusLabelBar(sender, e);
            }
        }

        private void minErrMenu(object sender, EventArgs e)
        {
            if (PB1.Image != null)
            {
                if (PB2.Image == null)
                {
                    greyscaleMenu(sender, e);
                }

                // valley
                byte[,] minErrArray = TH_MinErr.minErr(grayArray, histArrayG2);
                Bitmap minErrBitmap = new Bitmap(PB2.Image);
                minErrBitmap = Process.toBitmap(minErrArray);
                PB3.Image = minErrBitmap;

                TH = TH_MinErr.TH;
                statusLabelBar(sender, e);
            }
        }

        private void histEntrMenu(object sender, EventArgs e)
        {
            if (PB1.Image != null)
            {
                if (PB2.Image == null)
                {
                    greyscaleMenu(sender, e);
                }

                // entropy
                byte[,] entrHistArray = TH_EntrHist.entrHist(grayArray, histArrayG2);
                Bitmap entrHistBitmap = new Bitmap(PB2.Image);
                entrHistBitmap = Process.toBitmap(entrHistArray);
                PB3.Image = entrHistBitmap;

                TH = TH_EntrHist.TH;
                statusLabelBar(sender, e);
            }

        }

        private void otsuInsideMenu(object sender, EventArgs e)
        {
            if (PB1.Image != null)
            {
                if (PB2.Image == null)
                {
                    greyscaleMenu(sender, e);
                }

                // Otsu
                histArrayG2 = Hist.histogram(grayArray);
                byte[,] otsuInsideClassArray = TH_Otsu.otsuInsideClass(grayArray, histArrayG2);
                Bitmap otsuInsideClassBitmap = new Bitmap(PB2.Image);
                otsuInsideClassBitmap = Process.toBitmap(otsuInsideClassArray);
                PB3.Image = otsuInsideClassBitmap;
                TH = TH_Otsu.TH;
                statusLabelBar(sender, e);
            }
        }

        private void statusLabelBar(object sender, EventArgs e)
        {
            if (TH > 0)
            {
                SLB1.Text = String.Format("Threshold: {0}", TH);
                statusStrip1.Refresh();
            }
        }

    }
}
