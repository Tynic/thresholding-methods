﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AOSpokus
{
    class Process
    {
        public Process()
        {

        }

        /// <summary>
        /// Převod bitmapy na 3-rozměrné pole bajtů
        /// </summary>
        /// <param name="bitmap"></param>
        /// <returns>3-rozměrné pole bajtů</returns>
        public static byte[,,] getArray(Bitmap bitmap)
        {
            // vytvoř pole bajtů [šířka x výška x barva]
            byte[,,] bitmapArray = new byte[bitmap.Width, bitmap.Height, 3];

            Color color;
            byte red, green, blue;

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    color = bitmap.GetPixel(x, y);
                    red = color.R;
                    green = color.G;
                    blue = color.B;
                    bitmapArray[x, y, 0] = red;
                    bitmapArray[x, y, 1] = green;
                    bitmapArray[x, y, 2] = blue;
                }
            }
            return bitmapArray;
        }


        /// <summary>
        /// Uloží pole dat RGB do bitmapy
        /// </summary>
        /// <param name="bitmapArray">Pole s daty o údajích RGB</param>
        /// <returns>Objekt třídy bitmapa</returns>
        public static Bitmap SaveArrToBitmap(byte[,,] bitmapArray)
        {
            // pracovní objekt bitmapy
            Bitmap bitmap = new Bitmap(bitmapArray.GetLength(0), bitmapArray.GetLength(1), PixelFormat.Format24bppRgb);

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    // zápis barvy do bitmapy
                    bitmap.SetPixel(x, y, Color.FromArgb(bitmapArray[x, y, 0], bitmapArray[x, y, 1], bitmapArray[x, y, 2]));
                }
            }
            return bitmap;
        }

        /// <summary>
        /// Převod vstupního RGB pole do pole jasové složky
        /// </summary>
        /// <param name="picArray"></param>
        /// <returns>Pole [x,y] s převedeou jasovou složkou</returns>
        public static byte[,] grayscale(byte[,,] picArray)
        {
            // vytvářené pole obraz. dat o rozměru x,y vstupního pole
            byte[,] grayArray = new byte[picArray.GetLength(0), picArray.GetLength(1)];

            byte intensity = 0;
            int max = 0;
            byte red, green, blue;
            int[] histogram = new int[256];
            Array.Clear(histogram, 0, 256);     //vynuluj pole histogramu


            for (int y = 0; y < picArray.GetLength(1); y++)
            {
                for (int x = 0; x < picArray.GetLength(0); x++)
                {
                    // výpočet koeficientů jedn. složek barvy
                    red = (byte)(picArray[x, y, 0] * 0.299);
                    green = (byte)(picArray[x, y, 1] * 0.587);
                    blue = (byte)(picArray[x, y, 2] * 0.114);
                    intensity = (byte)(red + green + blue);
                    grayArray[x, y] = intensity;

                }
            }
            return grayArray;
        }

        /// <summary>
        /// Převod 2-rozměrného pole bajtů na bitmapu
        /// </summary>
        /// <param name="grayArray">pole bajtů</param>
        /// <returns>Objekt třídy bitmapa</returns>
        public static Bitmap toBitmap(byte[,] grayArray)
        {
            // pracovní objekt bitmapy
            Bitmap bitmap = new Bitmap(grayArray.GetLength(0), grayArray.GetLength(1));

            for (int y = 0; y < bitmap.Height; y++)
            {
                for (int x = 0; x < bitmap.Width; x++)
                {
                    // zápis barvy do bitmapy
                    bitmap.SetPixel(x, y, Color.FromArgb(grayArray[x, y], grayArray[x, y], grayArray[x, y]));
                }
            }
            return bitmap;
        }
    }
}
