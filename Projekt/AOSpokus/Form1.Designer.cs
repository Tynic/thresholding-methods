﻿namespace AOSpokus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grayscaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otsuToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.insideClassesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.betweenClassesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.valleyEmphasisToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.minimumErrorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.histogramEntropyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openFD = new System.Windows.Forms.OpenFileDialog();
            this.PB1 = new System.Windows.Forms.PictureBox();
            this.PB2 = new System.Windows.Forms.PictureBox();
            this.PB3 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.SLB1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB3)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.toolsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1531, 49);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveAsToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(75, 45);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(327, 46);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openMenu);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(327, 46);
            this.saveAsToolStripMenuItem.Text = "Save Output";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveMenu);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.grayscaleToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(80, 45);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // grayscaleToolStripMenuItem
            // 
            this.grayscaleToolStripMenuItem.Name = "grayscaleToolStripMenuItem";
            this.grayscaleToolStripMenuItem.Size = new System.Drawing.Size(327, 46);
            this.grayscaleToolStripMenuItem.Text = "Grayscale";
            this.grayscaleToolStripMenuItem.Click += new System.EventHandler(this.greyscaleMenu);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.otsuToolStripMenuItem1,
            this.minimumErrorToolStripMenuItem1,
            this.histogramEntropyToolStripMenuItem1});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(162, 45);
            this.toolsToolStripMenuItem.Text = "Threshold";
            // 
            // otsuToolStripMenuItem1
            // 
            this.otsuToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insideClassesToolStripMenuItem1,
            this.betweenClassesToolStripMenuItem1,
            this.valleyEmphasisToolStripMenuItem1});
            this.otsuToolStripMenuItem1.Name = "otsuToolStripMenuItem1";
            this.otsuToolStripMenuItem1.Size = new System.Drawing.Size(382, 46);
            this.otsuToolStripMenuItem1.Text = "Otsu";
            // 
            // insideClassesToolStripMenuItem1
            // 
            this.insideClassesToolStripMenuItem1.Name = "insideClassesToolStripMenuItem1";
            this.insideClassesToolStripMenuItem1.Size = new System.Drawing.Size(351, 46);
            this.insideClassesToolStripMenuItem1.Text = "Inside Classes";
            this.insideClassesToolStripMenuItem1.Click += new System.EventHandler(this.otsuInsideMenu);
            // 
            // betweenClassesToolStripMenuItem1
            // 
            this.betweenClassesToolStripMenuItem1.Name = "betweenClassesToolStripMenuItem1";
            this.betweenClassesToolStripMenuItem1.Size = new System.Drawing.Size(351, 46);
            this.betweenClassesToolStripMenuItem1.Text = "Between Classes";
            this.betweenClassesToolStripMenuItem1.Click += new System.EventHandler(this.otsuBetwMenu);
            // 
            // valleyEmphasisToolStripMenuItem1
            // 
            this.valleyEmphasisToolStripMenuItem1.Name = "valleyEmphasisToolStripMenuItem1";
            this.valleyEmphasisToolStripMenuItem1.Size = new System.Drawing.Size(351, 46);
            this.valleyEmphasisToolStripMenuItem1.Text = "Valley Emphasis";
            this.valleyEmphasisToolStripMenuItem1.Click += new System.EventHandler(this.valleyMenu);
            // 
            // minimumErrorToolStripMenuItem1
            // 
            this.minimumErrorToolStripMenuItem1.Name = "minimumErrorToolStripMenuItem1";
            this.minimumErrorToolStripMenuItem1.Size = new System.Drawing.Size(382, 46);
            this.minimumErrorToolStripMenuItem1.Text = "Minimum Error";
            this.minimumErrorToolStripMenuItem1.Click += new System.EventHandler(this.minErrMenu);
            // 
            // histogramEntropyToolStripMenuItem1
            // 
            this.histogramEntropyToolStripMenuItem1.Name = "histogramEntropyToolStripMenuItem1";
            this.histogramEntropyToolStripMenuItem1.Size = new System.Drawing.Size(382, 46);
            this.histogramEntropyToolStripMenuItem1.Text = "Histogram Entropy";
            this.histogramEntropyToolStripMenuItem1.Click += new System.EventHandler(this.histEntrMenu);
            // 
            // openFD
            // 
            this.openFD.FileName = "openFD";
            this.openFD.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // PB1
            // 
            this.PB1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.PB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PB1.Location = new System.Drawing.Point(0, 0);
            this.PB1.Margin = new System.Windows.Forms.Padding(0);
            this.PB1.Name = "PB1";
            this.PB1.Size = new System.Drawing.Size(510, 725);
            this.PB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB1.TabIndex = 0;
            this.PB1.TabStop = false;
            this.PB1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PB2
            // 
            this.PB2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PB2.Location = new System.Drawing.Point(510, 0);
            this.PB2.Margin = new System.Windows.Forms.Padding(0);
            this.PB2.Name = "PB2";
            this.PB2.Size = new System.Drawing.Size(510, 725);
            this.PB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB2.TabIndex = 3;
            this.PB2.TabStop = false;
            this.PB2.Click += new System.EventHandler(this.PB2_Click_1);
            // 
            // PB3
            // 
            this.PB3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PB3.Location = new System.Drawing.Point(1020, 0);
            this.PB3.Margin = new System.Windows.Forms.Padding(0);
            this.PB3.Name = "PB3";
            this.PB3.Size = new System.Drawing.Size(511, 725);
            this.PB3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PB3.TabIndex = 4;
            this.PB3.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.PB1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.PB2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.PB3, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 49);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1531, 725);
            this.tableLayoutPanel1.TabIndex = 5;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(40, 40);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SLB1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 752);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1531, 22);
            this.statusStrip1.TabIndex = 6;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // SLB1
            // 
            this.SLB1.Name = "SLB1";
            this.SLB1.Size = new System.Drawing.Size(0, 17);
            this.SLB1.Click += new System.EventHandler(this.statusLabelBar);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(16F, 31F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1531, 774);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 700);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AOS - Baierová";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PB3)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFD;
        private System.Windows.Forms.PictureBox PB1;
        private System.Windows.Forms.PictureBox PB2;
        private System.Windows.Forms.PictureBox PB3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ToolStripMenuItem grayscaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem otsuToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem insideClassesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem betweenClassesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem valleyEmphasisToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem minimumErrorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem histogramEntropyToolStripMenuItem1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel SLB1;
    }
}

